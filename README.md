# Applied Geophysics

These notebooks are intended to run in a separate conda environment, which can be **installed once** with

``` 
conda env create
```

and activated with

``` 
conda activate applied_geophysics
```