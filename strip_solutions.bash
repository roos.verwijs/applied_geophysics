#!/usr/bin/env bash

echo "Converting ${1}"

output=$(echo "${1}" | sed 's/\_Solution//g' | sed 's/\.ipynb//g')
echo "Outputting to $output"


# Run notebook and convert to python and latex
jupyter nbconvert --execute --inplace $1
jupyter nbconvert --to python $1
jupyter nbconvert --template=secnum.tplx --to pdf $1

# Same without solutions (needs proper cell tagging)
jupyter nbconvert --to notebook --TagRemovePreprocessor.remove_cell_tags="[\"solutions\", \"solutions\"]" --TagRemovePreprocessor.enabled=True $1 --output $(basename $output)
sed -i 's/\# Solution to/\#/g' $output.ipynb
jupyter nbconvert --to python $output
jupyter nbconvert --template=secnum.tplx --to pdf $output
